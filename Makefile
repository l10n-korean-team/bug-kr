# This makefile is really small. Most of the
# intelligence is in debian/debstd

all: bug-kr bugview-kr
	make -C po

version:
	touch debian/substvars
	VER=`dpkg-parsechangelog | grep 'Version: [0-9\.]*$$'` ; \
	VER=$${VER#Version: } ; \
	echo $$VER > $@

bug-kr: bug-kr.sh version
	sed -e "s/@VER@/$$(cat version)/g" < $< > $@
	chmod +x bug-kr

bugview-kr: bugview-kr.sh version
	sed -e "s/@VER@/$$(cat version)/g" < $< > $@
	chmod +x bugview-kr

po:
	make -C po

po/messages.pot: bug-kr bugview-kr
	./sh_xgettext $^ > $@

clean:
	rm -f bug-kr bugview-kr version build *~ core
	make -C po clean

install:
	install bug-kr $(DESTDIR)/usr/bin
	install bugview-kr $(DESTDIR)/usr/bin
	make -C po install DESTDIR=$(DESTDIR)
