#!/bin/bash
# bugview -- display debian bugs
#
#Author:	Adam Di Carlo <aph@debian.org>
#Copyright:	GPL v2
#Version:	@VER@
#
#Bugs:
#	view by maintainer name?

export TEXTDOMAIN=bug-kr

X11BROWSER="${X11BROWSER:-netscape}"
CONSOLEBROWSER="${CONSOLEBROWSER:-lynx}"
# normal bug site
BUGSERVER=http://www.debian-kr.org/Bugs/db
# *most* up-to-date site
#BUGSERVER=http://master.debian-kr.org/www-master/debian.org/Bugs/db/

# source our config file
[ -f /etc/bugview.conf ] && source /etc/bugview.conf

me=$(basename $0)

is_int() { expr "$1" : '[0-9][0-9]*$' > /dev/null; }

usage () {
echo $"Usage:  bugview-kr <bug>
View bugs from the Debian-KR BTS on the web.  <bug> can be either 
a package name or a bug number."
echo
echo -n $"The default X11 browser is "
echo "\`$X11BROWSER';"
echo -n $"the default console browser is "
echo "\`$CONSOLEBROWSER'."
echo $"You can override these defaults by setting
X11BROWSER and CONSOLEBROWSER, respectively."
}

while getopts h? opt; do
	case "$opt" in
		h|?)	usage
			exit;;
		*)	echo "Invalid argument: $opt" 1>&2
			usage 1>&2
			exit 1;;
	esac
done

shift `expr $OPTIND - 1`

if [ "$#" -ne 1 ]; then
    usage 1>&2
    exit 1
fi

if [ "$DISPLAY" ] && command -v "$X11BROWSER" &>/dev/null; then
    BROWSER="$X11BROWSER"
else
    BROWSER="$CONSOLEBROWSER"
fi

# determine whether this is a bug number or a package
if is_int $1; then
	URL="$BUGSERVER/${1::2}/$1.html"
else
	URL="$BUGSERVER/pa/l$1.html"
fi

# determine whether this is a bug number or a package
if [ "$DISPLAY" ]; then
    "$BROWSER" "$URL" &
else
    exec "$BROWSER" "$URL"
fi

