#!/bin/bash
#
# bug @VER@: Generates a bug report (needs bash >=2.0)
#
# Christoph Lameter, October 26, 1996
#
# Modified by Nicol�� Lichtmaier
#
set -e

# Compat tests
if [ "$BASH_VERSION" \< "2.01.1" ] ; then
	echo "$0: Needs Bash >= 2.01.1" >&2
	exit 1
fi
unset broken_collate || true
case b in [A-Z]) broken_collate=true ;; esac

export TEXTDOMAIN=bug-kr
YESNO=$"yYnN"

if [ -z "$EMAIL" ] ; then
	if  [ ! -r /etc/mailname ] ; then
		echo $"$0: Please set the environment variable EMAIL to contain your fully qualified e-mail address and try again." >&2
		exit 1
	fi
	CC="`id -un`"
	CC=`sed -ne "s/^$CC:[^:]*:[^:]*:[^:]*:\\([^,:]*\\),*:.*/\\1/p" \
		< /etc/passwd`" <$CC@`cat /etc/mailname`>"
else
	CC="$EMAIL"
fi

FROM="$CC"
if [ "$(type -t realpath 2> /dev/null)" = "" ]; then REALPATH=realp; else REALPATH=realpath ; fi

# Wait for a keypress and put it in $KEY
getkey()
{
	stty -icanon || true 2> /dev/null
	KEY=$(dd bs=1 count=1 2> /dev/null)
	stty icanon || true 2> /dev/null
	KEY="${KEY:0:1}"
	echo
}

# Usage: yesno <prompt> "yep"|"nop" (<- default)
#	output: REPLY
yesno()
{
	while true; do
		echo -n "$1"

		getkey

		# if 'n'
		if [ "$KEY" = "${YESNO:2:1}" -o "$KEY" = "${YESNO:3:1}" ]; then
			REPLY=nop
			return
		fi

		# if 'y'
		if [ "$KEY" = "${YESNO:0:1}" -o "$KEY" = "${YESNO:1:1}" ]; then
			REPLY=yep
			return
		fi

		# if \n
		if [ "$KEY" = "" ]; then
			REPLY=$2
			return
		fi
	done
}

realp()
{
	fname="$1"
	while [ -L "$fname" ]; do
		fname="$(command ls -l $fname)"
		fname="${fname#*\> }"
	done
	pushd $(dirname $fname) > /dev/null
	fname=$(pwd -P)/$(basename $fname)
	popd > /dev/null
	echo $fname
}

sendhelp()
{
echo "bug-kr v@VER@"
echo $"Usage: $1 [options] <packagename> [<text>]
A script to ease the reporting of bugs.

Options:
    -c          Do  not  include  configuration  files  in  the bug
                report.
    -d          Debug.  Dont send the mail to the bug tracking system,
                send it to postmaster@localhost instead.
    -f          <packagename> is a file belonging to the package
                and bug will search for it.
    -H special_header
                Adds  a  custom  header  to  generated mail. Can be
                repeated to add multiple custom headers.
    -m          Submit a \"maintonly\" bug report. Only sent  to  the
                maintainer not to the mailing list.
    -p          print. Do not send the bug report instead output it
                on stdout so that it can be redirected into a file.
    -q          Submit a quiet bug report (Just register. No e-mail
                forwarding to anyone)
    -s text     Set the Subject of the bug  report.  Otherwise  bug
                will ask for a short description of the problem.
    -S severity Set the severity level of the bug report.
    -x          Do not send a CC to the submitter of the bugreport
    -z          Do not compress the configfiles by  removing  empty
                lines and comments.
    -h          Shows this text.

Please don't report several unrelated bugs - especially ones in
different packages - in one message."
	exit 0
}

if [ "$1" = "--help" ]; then sendhelp ; fi

while getopts "cdfhH:mpqs:S:vzx-" OPT; do
	case $OPT in
		c)	NOCONFIG=1;;
		d)	DEBUG=1;;
		f)	SEARCH=1;;
		h)	sendhelp $0;;
		H)	CUSTOM_HEADER[${#CUSTOM_HEADER[*]}]=$OPTARG;;
		m)	MAINT=1;;
		p)	PRINT=1;;
		q)	QUIET=1;;
		s)	SUBJECT="$OPTARG"
			if [ -z "$SUBJECT" ]; then
				NOSUBJECT=yep
			fi
			;;
		S)	SEVERITY="$OPTARG";;
		v)	echo "bug @VER@" >&2
			exit 1;;
		x)	CC="";;
		z)	NOZAP=1;;
	        -)	break;;
	  	*)	echo $"Usage: $0 [options] <packagename> [<text>] .." >&2
		    	exit 1;;
        esac
done
shift $(($OPTIND - 1))

PACKAGE="$1"
shift || :
#if [ "$1" != "" ]; then
#	echo $"$0: Wrong number of arguments" >&2
#	exit 1
#fi

if [ "$UID" = "0" ]; then
	echo $"$0: Filing bug reports with superuser priviledges is insecure and not supported." >&2
	exit 1
fi

if [ "$SEARCH" ]; then
	if [ -z "$PACKAGE" ]; then
		echo $"$0: Must specify file name when using -f." 2>&2
		exit 1
	fi
	FILE="$PACKAGE"
	if [ ! -e "$FILE" ]; then
		OLDIFS="$IFS"
		IFS=":$IFS"
		fnfound=""
		for dir in $PATH; do
			if [ -e "$dir/$FILE" ]; then
				fnfound=yep
				FILE="$dir/$FILE"
				break;
			fi
		done
		IFS="$OLDIFS"
		if [ -z "$fnfound" ]; then
			echo
			echo $"$0: File \`$FILE' not found." 2>&2
			exit 1	
		fi
	fi
	FILE=$($REALPATH "$FILE")
	test "$PRINT" || echo -n $"Searching for package containing ${FILE}..."
	if [ "$FILE" = "" ]; then
		echo
		echo $"$0: Must include parameter when using -f." 2>&2
		exit 1
	fi
	PACKAGE=$(grep -l ^${FILE}\$ /var/lib/dpkg/info/*.list | head -1)
	if [ -z "$PACKAGE" ]; then
		echo
		echo $"$0: File \`$FILE' can't be found in any installed package." >&2
		exit 1
	fi
	PACKAGE=${PACKAGE##*/}
	PACKAGE=${PACKAGE%%.list}
	test "$PRINT" || echo $"done."
	if [ -z "$PRINT" ]; then
		echo $"\`$FILE' found in package \`$PACKAGE'"
		yesno $"Submit bug for this package? [Y|n] " yep
		if [ $REPLY = nop ]; then
			exit 0
		fi
	fi
fi

if [ "$PACKAGE" = "" ]; then
	echo $"Please state in which package you have found a problem, or...
type one of these bug categories:

bugs.debian-kr.org  The bug tracking system, @bugs.debian-kr.org
ftp.debian-kr.org   Problems with the FTP site
www.debian-kr.org   Problems with the WWW site
project-kr          Problems related to Project administration
general-kr          General problems (e.g., that many manpages are mode 755)
lists.debian-kr.org The mailing lists debian-*@lists.debian-kr.org."
	echo
	read -p $"package? " -er PACKAGE
	if [ "$PACKAGE" = "" ]; then
		echo $"Canceled!"
		exit 0
	fi
fi

# Checking for valid package name
i=$((${#PACKAGE}-1))

while [ $i -gt 0 -o $i -eq 0 ]; do
	case ${PACKAGE:$i:1} in
		" ")
			echo $"$0: Package name goes first, bug report last." >&2
			exit 1
			;;
		[A-Z])
			if [ "broken_collate" = "" ]; then
				echo $"$0: Packages names are formed with lower case letters." >&2
				exit 1
			fi
			;;
		[^-a-z0-9.+])
			echo $"$0: Invalid package name." >&2
			exit 1
			;;
	esac
	i=$(($i - 1))
done


#shift || true

if [ "$EDITOR" = "" ]; then
	EDITOR="$VISUAL"
	if [ "$EDITOR" = "" ]; then
		if [ -x /usr/bin/editor ]; then
			EDITOR="$($REALPATH /usr/bin/editor)"
		else
			if [ ! -x /bin/ae ]; then
				echo $"$0: Cannot locate a texteditor." >&2
				exit 1
			fi
			EDITOR="ae"
		fi
	fi
fi


checkconf()
{ 
# Takes two parameters filename and md5sum and checks if the file was modified
# If so outputs the filename
	if [ "$1" = "/etc/passwd" ]; then exit; fi
	if [ -r "$1" ]; then
		SUM=`md5sum $1`
		if [ "${SUM:0:32}" != "$2" ]; then
			echo "$1"
		fi
	else
		echo $1
	fi
}

checkconfs()
{
	read X
	while [ "$X" ]; do
		checkconf $X
		if ! read X ; then
			return
		fi
	done
}

pinfo()
{
	sed -n </var/lib/dpkg/status -e "/^Package: $1$/,/^Description: /p"
}

setsubject()
{
	if [ "$NOSUBJECT" != "yep" -a "$SUBJECT" = "" ]; then
		while [ "$SUBJECT" = "" ]; do
#			dialog --backtitle "Bug Reporting Tool - Debian/GNU Linux" \
#				--inputbox "Please describe your problems using $PACKAGE" \
#				8 78 2> /tmp/bugtmp...
#			SUBJECT=`cat /tmp/bugtmp...`
			echo $"Please describe your problems using $PACKAGE in one line."
			read -er SUBJECT ;
		done
		SUBJECT="$PACKAGE: $SUBJECT"
	fi
}

setseverity()
{
	local o="cCgGiInNwWhH"

	if [ -z "$SEVERITY" -a ! -z "$PRINT" ]; then
		SEVERITY=normal
	fi

	while [ "$SEVERITY" = "" ]; do
		echo -n $"Which should be the severity for this bug report? [cgiNw h=help] "
		getkey
		if [ "$KEY" = "${o:0:1}" -o "$KEY" = "${o:1:1}" ]; then
			SEVERITY=critical
		fi
		if [ "$KEY" = "${o:2:1}" -o "$KEY" = "${o:3:1}" ]; then
			SEVERITY=grave
		fi
		if [ "$KEY" = "${o:4:1}" -o "$KEY" = "${o:5:1}" ]; then
			SEVERITY=important
		fi
		if [ "$KEY" = "" -o "$KEY" = "${o:6:1}" -o "$KEY" = "${o:7:1}" ]; then
			SEVERITY=normal
		fi
		if [ "$KEY" = "${o:8:1}" -o "$KEY" = "${o:9:1}" ]; then
			SEVERITY=wishlist
		fi

		if [ "$KEY" = "${o:10:1}" -o "$KEY" = "${o:11:1}" ]; then
			echo
			echo $"c = critical
       makes unrelated software on the system (or the whole system)
       break, or causes serious data loss, or introduces a security
       hole on systems where you install the package.

g = grave
       makes the package in question unuseable or mostly so, or causes
       data loss, or introduces a security hole allowing access to the
       accounts of users who use the package.

i = important
       any other bug which makes the package unsuitable for release.

n = normal
       the default value, for normal bugs.

w = wishlist
       for any feature request, and also for any bugs that are very
       difficult to fix due to major design considerations."
       		echo
		fi

	done
}

template()
{
	if [ "$SUBJECT" ]; then
		echo "Subject: $SUBJECT"
	fi
 	cat <<-EOF
		Package: $PACKAGE
		Version: $VERSION
		Severity: $SEVERITY

		$*

		-- System Information
		Debian Release: `cat /etc/debian_version`
		Kernel Version: `uname -a`

	EOF
}

trap 'echo $"$0 interrupted." ; rm -f $TEMPS ; exit 0' SIGINT

TEMP1=$(tempfile -pbug)
TEMPS=$TEMP1
if [ -f /var/lib/dpkg/info/$PACKAGE.list ]; then
	test "$PRINT" || echo -n $"Getting package's info..."
	pinfo $PACKAGE >$TEMP1
	VERSION=`grep ^Version: $TEMP1`
	VERSION=${VERSION#Version: }
	DEPS=`grep Depends: $TEMP1` || DEPS=""
	if [ "$NOCONFIG" = "" ]; then
		if grep -q "Conffiles:" $TEMP1; then
			MODCONF=`sed -n <$TEMP1  -e '/Conffiles:/,/^[^ ].*\|^$/p'| egrep -v :\|^$ | checkconfs`
		fi
	fi
	test "$PRINT" || echo $"done."
	setsubject
	setseverity
	template "$*" >$TEMP1

	if [ "$DEPS" != "" ]; then
		test "$PRINT" || echo -n $"Checking ${PACKAGE}'s dependencies..."
		echo "Versions of the packages $PACKAGE depends on:" >>$TEMP1
		DEPS=`echo "$DEPS"|tr "|,\n" "  "|sed -e "s/ ([^)]*)//g" -e "s/Pre-Depends://g" -e "s/Depends://g" -e "s/  / /g" `
		pkgs=""
		vpkgs=""
		for i in $DEPS; do
			if [ -f /var/lib/dpkg/info/$i.list ]; then
				pkgs="$pkgs $i"
			else
				vpkgs="$vpkgs $i"
			fi
		done
		test "$pkgs" && dpkg -l $pkgs | tail +6 | sort -u >>$TEMP1
		unset pkgs
		for i in $vpkgs; do
			X=${i//+/\\\\+}
			TEMP2=$(tempfile -pbug)
			TEMPS="$TEMPS $TEMP2"
			perl -n000e "print if /^Provides:.* $X/mo && !/^Status: +[^ ]+ +[^ ]+ +config-files/mo" /var/lib/dpkg/status >$TEMP2
			if [ -s $TEMP2 ]; then
				REALPACKAGE=`head -1 $TEMP2`
				REALPACKAGE=${REALPACKAGE#Package: }
#				echo "$REALPACKAGE	$(grep '^Version:' $TEMP2) (Provides virtual package $i)" >>$TEMP1
				dpkg -l "$REALPACKAGE" | tail +6 >>$TEMP1
				echo "	^^^ (Provides virtual package $i)" >>$TEMP1
			else
				echo "$i	Not installed or no info" >>$TEMP1
			fi
			rm $TEMP2
		done
		unset vpkgs
		test "$PRINT" || echo $"done."
	fi

	if [ "$MODCONF" != "" ]; then
		test "$PRINT" || echo -n $"Including modified configuration files..."
		for i in $MODCONF; do
			if ls -lL "$i" | grep -qv ^.......r ; then
				continue
			fi
			echo -e "\n--- Begin $i (modified conffile)" >>$TEMP1
			if [ -r $i ]; then
				if file -L $i | grep -q "text"; then
					if [ "$NOZAP" ]; then
						cp $i $TEMP1
					else
						sed -e '/^#[^!]/d' -e '/^$/d' <$i >>$TEMP1
					fi
				else
					echo "*** Contents not text ***" >>$TEMP1
				fi
			else
				echo "Config file not present or no permissions for access" >>$TEMP1
			fi
			echo -e "\n--- End $i" >>$TEMP1
		done
		test "$PRINT" || echo $"done."
	fi
else
	VERSION="N/A"
	for i in base bootdisk rootdisk bugs.debian-kr.org ftp.debian-kr.org www.debian-kr.org manual project general kernel lists.debian-kr.org ; do
		if [ "$PACKAGE" = "$i" ]; then
			PSEUDO=yep
			VERSION=$(date -u +"%Y%m%d")
		fi
	done
	if [ "$PSEUDO" != "yep" ] && ! grep -q "^Package: $PACKAGE\>" /var/lib/dpkg/available; then
		echo $"Package $PACKAGE doesn't seem to exist!"
		yesno $"Submit bug anyway? [y|N] " nop
		if [ $REPLY = nop ]; then
			echo $"Bug report not sent"
			exit 1
		fi
	fi
	setsubject
	setseverity
	template "$*" >$TEMP1
fi

if [ -z "$1" -a -z "$PRINT" ]; then
	X=""
	while [ "$X" = "" ]; do
		TEMPBACKUP=$(tempfile -pbug)
		TEMPS="$TEMPS $TEMPBACKUP"
		cp $TEMP1 $TEMPBACKUP
		EDNAME="${EDITOR%% *}"
		EDNAME="${EDNAME##*/}"
		if [ "$EDNAME" = "joe" ]; then
			IFJOE="-skiptop 1"
			echo $"Reporting bug for package: $PACKAGE $VERSION"
			echo -ne \\033[2J\\033[1\;1H
		fi
		if [ "$EDNAME" = "joe" -o \
			"$EDNAME" = "jed" -o \
			"$EDNAME" = "emacs" -o \
			"$EDNAME" = "ae" -o \
			"$EDNAME" = "gnuclient" -o \
			"$EDNAME" = "elvis" -o \
			"$EDNAME" = "elvisnox" -o \
			"$EDNAME" = "nvi" -o \
			"$EDNAME" = "vim" -o \
			"$EDNAME" = "gvim" -o \
			"$EDNAME" = "vi" ]; then
			EDITARGS="+6"
		fi

		$EDITOR $IFJOE $EDITARGS $TEMP1 || true

		if cmp $TEMP1 $TEMPBACKUP >/dev/null; then
			echo $"Report unmodified, it won't be sent."
			rm $TEMP1 $TEMPBACKUP
			exit 1;
		fi
		yesno $"Do you really want to submit this bug report? [Y|n] " yep
		if [ $REPLY = nop ]; then
			cat "$TEMP1" >> /var/tmp/bug.dead
			echo $"Bug report cancelled (report text left in $TEMP1)."
			exit 0
		fi
		rm $TEMPBACKUP
		X="`head -1 $TEMP1`"
		X=${X#Subject: }
	done
	SUBJECT=$X
else
	if [ "$SUBJECT" = "" -a "$NOSUBJECT" != "yep" ]; then
		SUBJECT="$PACKAGE $VERSION"
	fi
fi

if [ "$PRINT" ]; then
	cat $TEMP1
	rm $TEMP1
	exit 0
fi

MADDR="submit@bugs.debian-kr.org"

if [ "$QUIET" ]; then
	MADDR="quiet@bugs.debian-kr.org"
fi

if [ "$MAINT" ]; then
	MADDR="maintonly@bugs.debian-kr.org"
fi

if [ "$DEBUG" ]; then
	MADDR="postmaster@localhost"
fi

{
	echo "From: $FROM"
	echo "Subject: $SUBJECT"
	echo "To: $MADDR"
	test "$CC" && echo "Bcc: $CC"
	echo "X-Mailer: bug @VER@"
	test "$REPLYTO" && echo "Reply-To: $REPLYTO"
	echo "MIME-Version: 1.0"
        echo "Content-Type: text/plain; charset=euc-kr"
        echo "Content-Transfer-Encoding: 8bit"
	i=0
	while [ $i -lt ${#CUSTOM_HEADER[*]} ] ; do
		echo ${CUSTOM_HEADER[$i]}
		i=$((i+1))
	done
	echo
	if [ -z "$1" ]; then
		sed <$TEMP1 -e "1d"
	else
		echo $@
	fi
} |
{
	{
		test -x /usr/sbin/sendmail && /usr/sbin/sendmail -t
	} ||
	{ 
		tee /var/tmp/${PACKAGE}.bug > /dev/null
		echo $"Bug report saved in /var/tmp/${PACKAGE}.bug"
		SAVED=true
	}
}

rm $TEMP1
if [ "$SAVED" = "" ]; then
	echo $"Bug Report submitted to $MADDR $CC"
fi

echo $"If you want to submit further information about this bug please wait
to receive the bug number by e-mail. Then mail any extra information to
nnnnn@bugs.debian-kr.org, where nnnnn is the bug number."
